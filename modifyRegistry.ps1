# @author TheBestPessimist

# working path
$path = "HKLM:\SYSTEM\CurrentControlSet\Control\Class\{4d36e972-e325-11ce-bfc1-08002be10318}\";

# needed in foreach, so that the "get/set -itemProperty" works
$path2 = "Microsoft.PowerShell.Core\Registry::"

$searchPath = (Get-ChildItem $path -ea 0).name;
# -ea 0 is equivalent to -ErrorAction SilentlyContinue

$searchSubject = "VLanFiltering"; 

# tell the user how many registry keys have been modified
$itemsModified = 0
foreach ($i in $searchPath)
{
	# current working item
	$workingPath = 	($path2 + $i);
	
	if( Get-itemProperty -path $workingPath -name $searchSubject -ErrorAction SilentlyContinue)
	{
		# modify that item
		Set-ItemProperty -path $workingPath -name $searchSubject -value 0;
		$itemsModified += 1;
		# echo ((get-item $workingPath));
	}
}

echo "`n$itemsModified registry keys have been modified.";
